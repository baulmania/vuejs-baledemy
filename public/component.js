const Home = {
    template: `<div>Home</div>`,
};

const About = {
    template: `<div>About</div>`,
};

const notFound = {
    template: `<div>Halaman Tidak Ditemukan!</div>`,
};
const Detail = {
    template: `<div>
        <template v-if="detailKelas">
            <img :src="url_gambar(detailKelas.gambar)" width="200"/>
            <h2>{{ detailKelas.judul }} - {{$route.params.idkelas}}</h2>
            <h4>{{ detailKelas.deskripsi }}</h4>
            <router-link to="/kelas">Kembali</router-link>
        </template>
        <p v-else>kelas tidak ada</p>
        </div>`,
    data: function () {
        return {
            detailKelas: {},
        };
    },
    created() {
        this.filterKelas();
    },
    methods: {
        filterKelas() {
            const vm = this;
            let id = vm.$route.params.idkelas;
            let kelasDetail = db.ref("kelas/" + id);
            kelasDetail.on("value", (item) => {
                this.detailKelas = item.val();
            });
        },
        url_gambar: function (gambar) {
            return gambar ? "../image/" + gambar : "";
        },
    },
};
const Kelas = {
    props: ["items", "kosong"],
    template: `
      <div>
      <h3>Tambah Kelas</h3>
        <form v-on:submit.prevent="submitkelas">
          <p><input type="text" placeholder="Nama Kelas" v-model="kelas.judul">
          <div class="error" v-if="error.judul"><small>{{ error.judul }}</small></div>
            </p>
          <div class="input-group">
              <label>Deskripsi</label><br>
              <textarea name="" id="" cols="30" rows="10" v-model="kelas.deskripsi"></textarea>
              <div class="error" v-if="error.deskripsi"><small>{{ error.deskripsi }}</small></div>
          </div>
          <div class="input-group">
            <p><img :src="priviewimg" v-if="priviewimg" width="200"/></p>
              <label>Masukkan Gambar</label><br>
              <input type="file" @change="upload" ref="gambar">
          </div>
          <button type="submit">Submit</button>
        </form>
        <h3>daftar kelas {{items.length}}</h3>
        <template v-if="items.length">
          <ul>
            <li v-for="(k,index) of items">
              <img :src="url_gambar(k.gambar)" width="200"/>
              <p>
                {{index + 1}} - {{k.judul}}
                <a href="" @click.prevent="$emit('hapuskelas', k.id )">hapus</a>
                <router-link :to="'/kelas/'+ k.id">lihat</router-link>
              </p>
            </li>
          </ul>
        </template>
        <li v-else>Kelas Belum Tersedia</li></div>`,
    data: function () {
        return {
            kelas: {
                judul: "",
                deskripsi: "",
                gambar: "",
            },
            priviewimg: "",
            error: {
                judul: "",
                deskripsi: "",
            },
        };
    },
    methods: {
        submitkelas: function () {
            if (this.kelas.judul == "") {
                this.error.judul = "Judul is required";
            } else {
                this.error.judul = "";
            }
            if (this.kelas.deskripsi == "") {
                this.error.deskripsi = "Deskripsi is required";
            } else {
                this.error.deskripsi = "";
            }

            if (this.kelas.judul && this.kelas.deskripsi) {
                const data = {
                    id: uuidv4(),
                    judul: this.kelas.judul,
                    deskripsi: this.kelas.deskripsi,
                    gambar: this.kelas.gambar,
                };
                this.$emit("tambahkelas", data);
                this.kelas.judul = "";
                this.kelas.deskripsi = "";
                this.kelas.gambar = "";
                this.priviewimg = "";
                this.$refs.gambar.value = "";
            }
        },
        upload: function (event) {
            const text = event.target.files[0].name;
            this.kelas.gambar = text;
            this.priviewimg = URL.createObjectURL(event.target.files[0]);
        },
        url_gambar: function (gambar) {
            return "image/" + gambar;
        },
    },
};

Vue.component("header-component", {
    props: ["nama"],
    template: `<header>
        <img src="../image/vuejs.jpg" width="300" alt="" />
        <p>The progressive JS Framework</p>
        <p>{{pesan}}</p>
        <p>{{'Hello '+nama}}</p>
      </header>`,
    data: function () {
        return {
            pesan: "Hello COmponent",
            gambar: "image/1.jpg",
        };
    },
});

Vue.component("footer-component", {
    template: `<footer id="footer">
        <slot></slot>
      </footer>`,
});
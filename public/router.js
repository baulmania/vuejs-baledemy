const routes = [
    { path: "/", component: Home },
    { path: "/about", component: About },
    { path: "/kelas", component: Kelas },
    { path: "/kelas/:idkelas", component: Detail },
    { path: "*", component: notFound },
];

const router = new VueRouter({
    mode: "history",
    routes, // short for `routes: routes`
});